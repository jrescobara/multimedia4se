package edu.fsu.cs.serene.down;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 */
public class VideoDownloader {

    public static void main(String[] args) throws IOException {

        Path inputFile = Paths.get("without_caption.csv");

        final File outputDir = new File("/largefs2/video/without_caption");
        if (!outputDir.exists()) {
            outputDir.mkdirs();
        }

        List<String> allLines = Files.readAllLines(inputFile);

        //File that will hold the completed video downloads.
        Path checkpointFile = Paths.get("completed_downloads.csv");
        List<String> lines;
        if (checkpointFile.toFile().exists()) {
            List<String> checkpointedLines = Files.readAllLines(checkpointFile);
            //Get the last completed download from the list. Find its location in the original corpus, then continue on the remaining tasks.
            String lastCompleted = checkpointedLines.get(checkpointedLines.size() - 1);
            int lastCompletedIndex = allLines.indexOf(lastCompleted);
            lines = allLines.subList(lastCompletedIndex + 1, allLines.size());
        } else {
            lines = allLines;
        }

        try (
                Writer log = new OutputStreamWriter(new FileOutputStream(new File(inputFile.toFile().getName() + ".log")));
                Writer err = new OutputStreamWriter(new FileOutputStream(new File(inputFile.toFile().getName() + ".err")));
                Writer completedItems = new OutputStreamWriter(new FileOutputStream(checkpointFile.toFile().getName()))) {

            ExecutorService es = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
            CompletionService cs = new ExecutorCompletionService(es);

            class MyTask implements Callable<Result> {

                String id;

                public MyTask(String id) {
                    this.id = id;
                }

                @Override
                public Result call() throws Exception {
                    Downloader downloader = new Downloader();
                    return downloader.downloadVideo(id, outputDir);
                }
            }

            //Submit tasks
            lines.stream().forEach((line) -> {
                cs.submit(new MyTask(line.trim()));
            });

            //Get tasks' results. Write in .log or .err 
            int n = lines.size();
            for (int i = 0; i < n; i++) {
                try {
                    Result result = (Result) cs.take().get();
                    if (result.isSuccess()) {
                        log.write(result.getId() + " OK\n");
                        completedItems.write(result.getId()+"\n");
                    } else {
                        log.write(result.getId() + " " + result.getMessage() + "\n");
                        err.write(result.getId() + "\n");
                        err.flush();
                    }
                } catch (InterruptedException | ExecutionException ex) {
                    log.write("EXECUTION ERROR\n");
                }
                log.flush();
                completedItems.flush();
            }
            es.shutdown();
        }
    }
}
