package edu.fsu.cs.serene.down;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 */
class Result {

    private final String id;
    private final String message;
    private final boolean success;

    public Result(String id, String message, boolean success) {
        this.id = id;
        this.message = message;
        this.success = success;
    }

    public String getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public boolean isSuccess() {
        return success;
    }

}
