package edu.fsu.cs.serene.down;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 */
class Downloader {

    public Result downloadVideo(String id, File outputDir) {
        try {

            String command = "youtube-dl "
                    + "-k -f bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best "
                    + " -o " + outputDir.getAbsolutePath() + File.separator + "%(id)s.%(ext)s"
                    + " https://www.youtube.com/watch?v=" + id;
            Process p = Runtime.getRuntime().exec(command);
            boolean terminated = p.waitFor(30, TimeUnit.MINUTES);
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
            return new Result(id, null, p.exitValue() == 0 && terminated);
        } catch (IOException | InterruptedException ex) {
            return new Result(id, ex.getMessage(), false);
        }
    }
}
