package edu.cs.fsu.serene.client;

import io.cortical.retina.client.LiteClient;
import io.cortical.retina.rest.ApiException;
import java.util.List;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 */
public class CorticalClient {

    private final LiteClient client;

    public CorticalClient() {
        client = new LiteClient("68440490-e9fb-11e6-8782-2f4eaf4c41b5");
    }

    public List<String> getKeywords(String text) throws ApiException {
        return client.getKeywords(text);
    }
}
