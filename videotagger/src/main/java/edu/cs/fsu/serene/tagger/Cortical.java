package edu.cs.fsu.serene.tagger;

import edu.cs.fsu.serene.client.CorticalClient;
import io.cortical.retina.rest.ApiException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 */
class Cortical implements Technique {

    private final CorticalClient client;

    public Cortical() {
        client = new CorticalClient();
    }

    @Override
    public List<String> getTags(String text) {
        try {
            return client.getKeywords(text);
        } catch (ApiException ex) {
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public List<String> getTags(File file) {
        try {

            String text = new String(Files.readAllBytes(file.toPath()));
            return getTags(text);
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

}
