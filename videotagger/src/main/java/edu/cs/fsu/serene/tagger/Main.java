package edu.cs.fsu.serene.tagger;

import edu.cs.fsu.serene.ir.index.Similarities;
import edu.cs.fsu.serene.ir.util.DocumentParseException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import org.apache.lucene.search.similarities.Similarity;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 */
public class Main {

    public static void main(String[] args) throws Exception {
        File queryDir = new File("F:\\transcripts\\fulllabeled");
        File corpusDir = new File("");

        //doCortical(queryDir);
        doLucene(corpusDir, queryDir, Similarities.getLMDirichlet());
    }
    
    
    private static void doLucene(File corpusDir, File queryDir, Similarity similarity) throws IOException, DocumentParseException {
        
        Lucene lucene = (Lucene)ServiceLocator.getLuceneIndex();        
        lucene.index(corpusDir.listFiles(), new VideoDocument(), similarity);
        
        //Let's tag
        for (File file : queryDir.listFiles()) {
            String text = new String(Files.readAllBytes(file.toPath()));
            text = TagRemover.removeTerrierTags(text).toLowerCase();
            
            lucene.setSimilarity(similarity);
            List<String> tags = lucene.getTags(text);
            tags.stream().forEach((tag) -> {
                System.out.println(file.getName().replace(".txt", "") + "," + tag);
            });
        }
    }

    private static void doCortical(File corpusDir) throws IOException {
        //Let's tag
        for (File file : corpusDir.listFiles()) {
            String text = new String(Files.readAllBytes(file.toPath()));
            text = TagRemover.removeTerrierTags(text).toLowerCase();
            List<String> tags = ServiceLocator.getCortical().getTags(text);
            tags.stream().forEach((tag) -> {
                System.out.println(file.getName().replace(".txt", "") + "," + tag);
            });
        }
    }
}
