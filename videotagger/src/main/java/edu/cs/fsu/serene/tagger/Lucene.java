package edu.cs.fsu.serene.tagger;

import edu.cs.fsu.serene.ir.index.DocumentCreator;
import edu.cs.fsu.serene.ir.index.LuceneIndex;
import edu.cs.fsu.serene.ir.index.Result;
import edu.cs.fsu.serene.ir.util.DocumentParseException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.similarities.Similarity;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 */
public class Lucene implements Technique {

    private final LuceneIndex client;
    private Similarity similarity;

    public Lucene() {
        client = new LuceneIndex();
    }

    public void index(File[] corpus, DocumentCreator parser, Similarity similarity) throws DocumentParseException, IOException {
        client.index(corpus, parser, similarity);
    }

    @Override
    public List<String> getTags(String text) {
        try {
            List<Result> results = client.query(text, "text", 10, similarity, SOTagResult.class);
            List<String> output = new ArrayList<>();
            results.stream().forEach((result) -> {
                output.add(((SOTagResult) result).getTag());
            });
            return output;
        } catch (ParseException | IOException | InstantiationException | IllegalAccessException ex) {
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public List<String> getTags(File file) {
        try {
            String text = new String(Files.readAllBytes(file.toPath()));
            return getTags(text);
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    public void setSimilarity(Similarity similarity) {
        this.similarity = similarity;
    }
}
