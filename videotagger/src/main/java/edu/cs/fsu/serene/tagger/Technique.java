package edu.cs.fsu.serene.tagger;

import java.io.File;
import java.util.List;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 */
interface Technique {

    public List<String> getTags(String text);

    public List<String> getTags(File file);

}
