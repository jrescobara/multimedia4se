package edu.cs.fsu.serene.tagger;

import edu.cs.fsu.serene.ir.index.DocumentCreator;
import edu.cs.fsu.serene.ir.util.DocumentParseException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 */
public class VideoDocument implements DocumentCreator {

    @Override
    public Document createDocument(File file) throws DocumentParseException {
        Document document = new Document();
        try {
            document.add(new TextField("text", new String(Files.readAllBytes(file.toPath())).trim(), Field.Store.YES));
        } catch (IOException ex) {
            throw new DocumentParseException("Document unable to be indexed: " + ex.getMessage());
        }

        //TODO: Smarter way to remove extension (maybe commons.io)
        document.add(new StringField("docid", file.getName().replace(".txt", ""), Field.Store.YES));
        return document;
    }
}
