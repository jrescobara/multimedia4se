package edu.cs.fsu.serene.tagger;

import edu.cs.fsu.serene.ir.index.Result;
import java.io.IOException;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 */
public class SOTagResult extends Result {

    private String tag;

    @Override
    public void parseLuceneResult(IndexSearcher searcher, ScoreDoc document) {
        try {
            Document d = searcher.doc(document.doc);
            tag = d.get("tag");
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    public String getTag() {
        return tag;
    }

}
