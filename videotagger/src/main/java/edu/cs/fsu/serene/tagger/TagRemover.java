package edu.cs.fsu.serene.tagger;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 */
class TagRemover {

    public static String removeTerrierTags(String text) {
        return text.replace("<DOC>", "")
                .replace("</DOC>", "")
                .replace("<TRANSCRIPT>", "")
                .replace("</TRANSCRIPT>", "")
                .replace("<DESCRIPTION>", "")
                .replace("</DESCRIPTION>", "")
                .replace("<TITLE>", "")
                .replace("</TITLE>", "")
                .replace("<ID>", "")
                .replace("</ID>", "").trim();
    }

}
