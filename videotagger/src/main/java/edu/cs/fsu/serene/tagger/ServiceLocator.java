
package edu.cs.fsu.serene.tagger;

import edu.cs.fsu.serene.ir.index.LuceneIndex;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 */
 class ServiceLocator {
    
    
     public static Technique getCortical(){
         return new Cortical();
     }
     public static Technique getLuceneIndex(){
         return new Lucene();
     }
     
    
}
