package edu.cs.fsu.serene.ir.index;

import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;
import org.apache.lucene.search.similarities.Similarity;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 */
public final class Similarities {

    private Similarities() {
    }

    public static Similarity getBM25(){
        return new BM25Similarity();
    }
    
    public static Similarity getLMDirichlet(){
        return new LMDirichletSimilarity();
    }
    
    public static Similarity getLMJelinekMercerSimilarity(){
        return new LMDirichletSimilarity();
    }    
}
