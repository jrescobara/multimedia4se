package edu.cs.fsu.serene.ir.util;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 */
public class DocumentParseException extends Exception {

    /**
     * Creates a new instance of <code>ParseException</code> without detail
     * message.
     */
    public DocumentParseException() {
    }

    /**
     * Constructs an instance of <code>ParseException</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     */
    public DocumentParseException(String msg) {
        super(msg);
    }
}
