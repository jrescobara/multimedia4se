package edu.cs.fsu.serene.ir.index;

import edu.cs.fsu.serene.ir.util.DocumentParseException;
import java.io.File;
import org.apache.lucene.document.Document;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 */
public interface DocumentCreator {
    
    public Document createDocument(File file) throws DocumentParseException;
    
}
