package edu.cs.fsu.serene.ir.pre;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 *
 */
public class Dictionaries {

    public static Dictionary createFromFile(String filename) {
        return new FileDictionary(filename);
    }

}
