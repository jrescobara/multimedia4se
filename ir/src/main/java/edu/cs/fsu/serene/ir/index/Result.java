/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.cs.fsu.serene.ir.index;

import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 */
public abstract class Result {

    public Result() {
    }

    public abstract void parseLuceneResult(IndexSearcher searcher, ScoreDoc document);

}
