package edu.cs.fsu.serene.ir.pre;

import java.util.Collection;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 *
 */
public interface Dictionary {
    
    public Collection<String> words();
        
}
