package edu.cs.fsu.serene.ir.index;

import edu.cs.fsu.serene.ir.util.DocumentParseException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 */
public class LuceneIndex {

    private Directory index;
    private Analyzer analyzer;

    public void index(File[] files, DocumentCreator parser, Similarity similarity) throws DocumentParseException, IOException {
        if (index == null) {
            index = new RAMDirectory();
            analyzer = new StandardAnalyzer();
        }

        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        config.setSimilarity(similarity);

        try (IndexWriter writer = new IndexWriter(index, config)) {
            for (File file : files) {
                Document document = parser.createDocument(file);
                writer.addDocument(document);
            }
        }
    }

    public List<Result> query(String query, String field, int k, Similarity similarity, Class<? extends Result> resultType) throws ParseException, IOException, InstantiationException, IllegalAccessException {
        if (index == null) {
            throw new IllegalStateException("Index has not been initializated");
        }

        BooleanQuery.setMaxClauseCount(10000);
        Query q = new BooleanQuery.Builder()
                .add(new BooleanClause(new MatchAllDocsQuery(), BooleanClause.Occur.MUST))
                .add(new BooleanClause(new QueryParser(field, analyzer).parse(query), BooleanClause.Occur.SHOULD))
                .build();
        IndexReader reader = DirectoryReader.open(index);

        IndexSearcher searcher = new IndexSearcher(reader);
        searcher.setSimilarity(new BM25Similarity());

        TopDocs results = searcher.search(q, reader.numDocs());
        ScoreDoc[] documents = results.scoreDocs;

        List<Result> output = new ArrayList<>();

        for (ScoreDoc document : documents) {
            Result result = resultType.newInstance();
            result.parseLuceneResult(searcher, document);
            output.add(result);
        }
        return output;
    }

    public void clean() throws IOException {
        index.close();
    }

}
