package edu.cs.fsu.serene.ir.pre;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 *
 */
class FileDictionary implements Dictionary {

    private final String filename;

    public FileDictionary(String filename) {
        this.filename = filename;
    }

    @Override
    public Collection<String> words() {
        try {
            return Files.readAllLines(Paths.get(ClassLoader.getSystemResource(filename).toURI()), Charset.forName("UTF-8"));
        } catch (IOException | URISyntaxException ex) {
            throw new IllegalArgumentException(ex.getMessage());
        }
    }
}
