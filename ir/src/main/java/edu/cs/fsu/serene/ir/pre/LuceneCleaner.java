package edu.cs.fsu.serene.ir.pre;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.miscellaneous.WordDelimiterFilterFactory;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.analysis.util.TokenFilterFactory;

/**
 * TODO: Terrible class. Needs refactoring asap
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 *
 */
public final class LuceneCleaner {

    private final Map<String, String> args = new HashMap<>();

    private final List<Dictionary> stopwords = new ArrayList<>();

    public LuceneCleaner() {
        args.put("generateWordParts", "1");
        args.put("generateNumberParts", "1");
        args.put("splitOnCaseChange", "1");
        args.put("splitOnNumerics", "1");
        args.put("stemEnglishPossessive", "1");
    }

    public LuceneCleaner(boolean generateWordParts, boolean generateNumberParts, boolean splitOnCaseChange, boolean splitOnNumerics, boolean stemEnglishPossessive) {
        if (generateWordParts) {
            args.put("generateWordParts", "1");
        }
        if (generateNumberParts) {
            args.put("generateNumberParts", "1");
        }
        if (splitOnCaseChange) {
            args.put("splitOnCaseChange", "1");
        }
        if (splitOnNumerics) {
            args.put("splitOnNumerics", "1");
        }
        if (stemEnglishPossessive) {
            args.put("stemEnglishPossessive", "1");
        }
    }

    /**
     * 
     * @param input Text to be cleans
     * @return <code>null</code> if there are not relevant terms. 
     * @throws IOException  Should not happen. 
     */
    public String processStringPorter(String input) throws IOException {
        Analyzer analyzer = new StandardAnalyzer();
        TokenStream tokenStream = analyzer.tokenStream(null, new StringReader(input));
        TokenFilterFactory fact = new WordDelimiterFilterFactory(args);
        tokenStream = fact.create(tokenStream);
        tokenStream = new LowerCaseFilter(tokenStream);
        for (Dictionary list : stopwords) {
            tokenStream = new StopFilter(tokenStream, new CharArraySet(list.words(), true));
        }

        tokenStream = new NumberFilter(tokenStream);
        tokenStream = new PorterStemFilter(tokenStream);

        StringBuilder sb = new StringBuilder();
        CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);
        tokenStream.reset();
        while (tokenStream.incrementToken()) {
            if (sb.length() > 0) {
                sb.append(" ");
            }
            String term = charTermAttribute.toString().replaceAll("\\W", "");
            if (term.trim().isEmpty()) {
                continue;
            }
            sb.append(term);
        }
        String output = sb.toString().trim();
        if (output.isEmpty()) {
            return null;
        } else {
            return output;
        }
    }
    
    public void addStopWordsList(Dictionary list){
        stopwords.add(list);
    }

}
