package edu.cs.fsu.serene.ir.tool;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 */
public class Metrics {

    public double cosineSim(double[] v1, double[] v2) {
        if (v1.length != v2.length) {
            throw new IllegalArgumentException("Vectors' lenght must match");
        }
        double dotProduct = 0.0;
        double normV1 = 0.0;
        double normV2 = 0.0;
        for (int i = 0; i < v1.length; i++) {
            dotProduct += v1[i] * v2[i];
            normV1 += Math.pow(v1[i], 2);
            normV2 += Math.pow(v2[i], 2);
        }
        return dotProduct / (Math.sqrt(normV1) * Math.sqrt(normV2));
    }

}
