package edu.cs.fsu.serene.crw.sodoc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * This is the StackOverflow documentation's crawler. Right now works only for
 * the "Java Language" Tag
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 *
 */
class SODCrawler {

    /**
     * 
     * @return List of topics for Java Language Tag
     * @throws IOException When it could not establish a connection
     */
    public List<Topic> crawl() throws IOException {
        final String SO = "http://stackoverflow.com";
        final String ROOT = SO + "/documentation?page=";

        List<Topic> topics = new ArrayList<>();

        for (int i = 1; i <= 1; i++) {
            Document doc;
            try {
                doc = Jsoup.connect(ROOT + "i").get();
            } catch (IOException e) {
                continue;
            }
            Elements card_tops = doc.getElementsByClass("card-top");
            for (Element card_top : card_tops) {
                Element a = card_top.getElementsByTag("a").get(0);
                String tag = a.text();
                if (!tag.equals("Java Language")) {
                    continue;
                }
                String url = SO + a.attr("href");
                final Tag newTag = new Tag(tag, url);

                Document tdoc;
                try {
                    tdoc = Jsoup.connect(newTag.url).get();
                } catch (IOException e) {
                    throw e;
                }

                Element pager = tdoc.getElementsByClass("pager").get(0);
                Elements items = pager.getElementsByTag("a");
                Element last = items.get(items.size() - 2);

                int number = Integer.parseInt(last.text());

                for (int j = 1; j <= number; j++) {
                    try {
                        tdoc = Jsoup.connect(newTag.url + "?page=" + j).get();
                    } catch (IOException e) {
                        continue;
                    }
                    Elements tlinks = tdoc.getElementsByClass("doc-topic-link");
                    for (Element tlink : tlinks) {
                        final String topic = tlink.text();
                        final String turl = SO + tlink.attr("href");

                        final Topic newTopic = new Topic(topic, turl);
                        topics.add(newTopic);

                        Document edoc;
                        try {
                            edoc = Jsoup.connect(turl).get();
                        } catch (IOException e) {
                            continue;
                        }

                        Elements examples = edoc.getElementsByClass("example");
                        examples.stream().forEach((example) -> {
                            String title = example.getElementsByClass("doc-example-link").get(0).text();
                            String text = example.getElementsByClass("example-body-html").get(0).text();
                            newTopic.addExample(new Example(title, text));
                        });
                    }
                }
                break;
            }
        }

        return topics;
    }
}
