package edu.cs.fsu.serene.crw.sodoc;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 *
 */
public class Main {

    public static void main(String[] args) throws IOException {

        //Topic-level
        File topicsDir = new File("topics");
        if (!topicsDir.exists()) {
            topicsDir.mkdirs();
        }
        //Example level
        File examplesDir = new File("examples");
        if (!examplesDir.exists()) {
            examplesDir.mkdirs();
        }

        int topicCount = 1;
        int exampleCount = 1;
        SODCrawler soc = new SODCrawler();
        List<Topic> topics = soc.crawl();
        try (Writer topicw = new FileWriter("topicIDs"); Writer examplew = new FileWriter("exampleIDs")) {
            for (Topic topic : topics) {

                topicw.write(topicCount + ",\"" + topic.getName() + "\"\n");
                File ftopic = new File(topicsDir.getAbsolutePath() + File.separator + topicCount + ".txt");

                FileUtils.write(ftopic, topic.getName() + "\n", "UTF-8", true);

                for (Example example : topic.getExamples()) {
                    examplew.write(exampleCount + ",\"" + example.getTitle() + "\"\n");
                    File fexample = new File(examplesDir.getAbsolutePath() + File.separator + exampleCount + ".txt");

                    FileUtils.write(ftopic, example.getTitle() + "\n", "UTF-8", true);
                    FileUtils.write(ftopic, example.getText() + "\n", "UTF-8", true);

                    FileUtils.write(fexample, example.getTitle() + "\n", "UTF-8", true);
                    FileUtils.write(fexample, example.getText() + "\n", "UTF-8", true);

                    exampleCount++;
                }
                topicCount++;
            }
        }
    }
}
