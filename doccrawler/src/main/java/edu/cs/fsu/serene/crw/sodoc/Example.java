/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.cs.fsu.serene.crw.sodoc;

import java.io.Serializable;

/**
 *
 * @author Javier
 */
public class Example implements Serializable {

    private final String title;
    private final String text;

    public Example(String title, String text) {
        this.title = title;
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }
}
