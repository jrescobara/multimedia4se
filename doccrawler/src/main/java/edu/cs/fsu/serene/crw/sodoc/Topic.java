package edu.cs.fsu.serene.crw.sodoc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Javier
 */
class Topic implements Serializable {

    private final String name;
    private final String url;

    private final List<Example> examples;

    public Topic(String name, String url) {
        this.name = name;
        this.url = url;
        examples = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public void addExample(Example example) {
        examples.add(example);
    }

    public List<Example> getExamples() {
        return examples;
    }

}
