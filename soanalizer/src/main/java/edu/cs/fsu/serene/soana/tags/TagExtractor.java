package edu.cs.fsu.serene.soana.tags;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 */
public class TagExtractor {

    public static void main(String[] args) throws Exception {
        File soFile = new File("F:\\Posts.xml");

        final Map<String, Integer> javaTags = new HashMap<>();

        double total = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(soFile))) {
            while (reader.ready()) {
                final String line = reader.readLine().trim();
                if (line.contains("<?xml version=\"1.0\" encoding=\"utf-8\"?>") || line.endsWith("posts>")) {
                    continue;
                }
                List<String> tags = Analizer.getTags(line);
                if (tags == null) {
                    continue;
                }
                main:
                for (String tag : tags) {
                    if (tag.contains("javascript")) {
                        continue;
                    }

                    if (tag.contains("java")) {
                        tags.stream().map((tag1) -> {
                            if (!javaTags.containsKey(tag1)) {
                                javaTags.put(tag1, 0);
                            }
                            return tag1;
                        }).forEach((tag1) -> {
                            javaTags.put(tag1, javaTags.get(tag1) + 1);
                        });
                        break;
                    }
                }
                total++;
                if (total % 1000000 == 0) {
                    System.out.println("Lines " + total + " Tags " + javaTags.size());
                }
            }
        }
        javaTags.entrySet().stream().forEach((entry) -> {
            System.out.println(entry.getKey() + "," + entry.getValue());
        });
    }
}
