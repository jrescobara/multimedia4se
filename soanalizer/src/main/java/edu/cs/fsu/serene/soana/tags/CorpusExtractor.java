/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.cs.fsu.serene.soana.tags;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.apache.commons.io.FileUtils;
import org.w3c.dom.NamedNodeMap;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 */
public class CorpusExtractor {

    public static void main(String[] args) throws Exception {
        File soFile = new File("F:\\Posts.xml");
        File outputDir = new File("F:\\SO_corpus");
        if (!outputDir.exists()) {
            outputDir.mkdirs();
        }

        Set<Double> acceptedAnswerIds = new HashSet<>();
        Map<String, File> files = new TreeMap<>();
        Map<Double, List<String>> mapWithTags = new TreeMap<>();
        final Charset charset = Charset.forName("UTF-8");

        double num = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(soFile))) {
            while (reader.ready()) {
                final String line = reader.readLine().trim();
                if (line.contains("<?xml version=\"1.0\" encoding=\"utf-8\"?>") || line.endsWith("posts>")) {
                    continue;
                }
                try {
                    NamedNodeMap nnm = Analizer.getdata(line);

                    int postType = Integer.parseInt(nnm.getNamedItem("PostTypeId").getNodeValue());
                    double id = Double.parseDouble(nnm.getNamedItem("Id").getNodeValue());
                    List<String> tags;
                    StringBuilder sb = new StringBuilder();
                    switch (postType) {
                        case 1://Question
                            tags = getJavaTags(nnm);
                            if (tags == null) {
                                continue;
                            }
                            mapWithTags.put(id, tags);

                            sb.append(nnm.getNamedItem("Title").getNodeValue());
                            sb.append("\n");
                            sb.append(nnm.getNamedItem("Body").getNodeValue().replaceAll("\\<[^>]*>", ""));
                            sb.append("\n");

                            try {
                                Double acceptedId = Double.parseDouble(nnm.getNamedItem("AcceptedAnswerId").getNodeValue());
                                acceptedAnswerIds.add(acceptedId);
                            } catch (NullPointerException ex) { //Maybe it does not have one
                            }

                            for (String tag : tags) {
                                if (tag.trim().isEmpty()) {
                                    continue;
                                }
                                if (!files.containsKey(tag)) {
                                    files.put(tag, new File(outputDir + File.separator + tag + ".txt"));
                                }
                                FileUtils.write(files.get(tag), sb.toString(), charset, true);
                            }

                            break;
                        case 2://Answer
                            if (acceptedAnswerIds.contains(id)) {
                                sb.append(nnm.getNamedItem("Body").getNodeValue().replaceAll("\\<[^>]*>", ""));
                                sb.append("\n");

                                Double parentId = Double.parseDouble(nnm.getNamedItem("ParentId").getNodeValue());
                                tags = mapWithTags.get(parentId);

                                for (String tag : tags) {
                                    if (tag.trim().isEmpty()) {
                                        continue;
                                    }
                                    FileUtils.write(files.get(tag), sb.toString(), charset, true);
                                }
                            }

                            break;
                        default:
                            System.err.println("Type " + postType);
                            break;
                    }

                } catch (NullPointerException ex) {
                    System.err.println(ex.getMessage() + " " + line);
                }
                num++;
                if (num % 1000000 == 0) {
                    System.out.println(num);
                }
            }
        }
    }

    private static List<String> getJavaTags(NamedNodeMap nnm) {
        try {
            String tags = nnm.getNamedItem("Tags").getNodeValue();
            List<String> list = Arrays.asList(tags.replace("<", "").replace(">", " ").split(" ", -1));
            for (String tag : list) {
                if (tag.contains("javascript")) {
                    continue;
                }
                if (tag.contains("java")) {
                    return list;
                }
            }
            return null;
        } catch (NullPointerException ex) {
            return null;
        }
    }
}
