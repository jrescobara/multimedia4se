package edu.cs.fsu.serene.soana.tags;

import java.io.StringReader;
import java.util.Arrays;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Javier
 */
class Analizer {

    public static List<String> getTags(String line) throws Exception {
        InputSource is = new InputSource(new StringReader(line));

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(is);

        Node n = doc.getElementsByTagName("row").item(0);
        NamedNodeMap nnm = n.getAttributes();

        if (Integer.parseInt(nnm.getNamedItem("PostTypeId").getNodeValue()) != 1) {
            return null;
        }

        try {
            String tags = nnm.getNamedItem("Tags").getNodeValue();
            return Arrays.asList(tags.replace("<", "").replace(">", " ").split(" ", -1));
        } catch (NullPointerException ex) {
            return null;
        }
    }

    public static NamedNodeMap getdata(String line) throws Exception {
        InputSource is = new InputSource(new StringReader(line));

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(is);

        Node n = doc.getElementsByTagName("row").item(0);
        return n.getAttributes();
    }
}
