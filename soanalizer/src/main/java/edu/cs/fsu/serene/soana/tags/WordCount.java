/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.cs.fsu.serene.soana.tags;

import com.koloboke.collect.map.hash.HashObjIntMaps;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

/**
 *
 * @author Javier Escobar-Avila <escobara@cs.fsu.edu>
 */
public class WordCount {

    public static void main(String[] args) throws Exception {
        File inputDir = new File("F:\\SO_corpus");
        File outputDir = new File("F:\\SO_WordCount");
        if (!outputDir.exists()) {
            outputDir.mkdirs();
        }

        final List<String> stopWords = new ArrayList<>();
        addStopWords(stopWords, new File("src/main/resources/java-stopwords.txt"));
        addStopWords(stopWords, new File("src/main/resources/terrier-stopwords.txt"));

//        ExecutorService es = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        ExecutorService es = Executors.newFixedThreadPool(1);
        CompletionService<Boolean> cs = new ExecutorCompletionService<>(es);

        for (File file : inputDir.listFiles()) {
            cs.submit(new WordCountTask(file, new File(outputDir.getAbsolutePath() + File.separator + file.getName()), stopWords));
        }

        int total = inputDir.listFiles().length;
        int correct = 0;
        int error = 0;
        for (int i = 0; i < total; i++) {
            if (cs.take().get()) {
                correct++;
            } else {
                error++;
            }
            if (i % 1000 == 0) {
                System.out.println("Analyzed " + i + " out of " + total);
            }
        }
        es.shutdown();
    }

    private static void addStopWords(List<String> stopwords, File input) throws IOException {
        stopwords.addAll(Files.readAllLines(input.toPath()));
    }
}

class WordCountTask implements Callable<Boolean> {

    private final File file;
    private final File output;
    private final List<String> stopWords;

    public WordCountTask(File file, File output, List<String> stopWords) {
        this.file = file;
        this.output = output;
        this.stopWords = stopWords;
    }

    @Override
    public Boolean call() throws Exception {
        try {
            StandardAnalyzer analyzer = new StandardAnalyzer(new CharArraySet(stopWords, true));
            Map<String, Integer> map = HashObjIntMaps.newMutableMap();

            TokenStream stream = analyzer.tokenStream(null, new FileReader(file));
            stream.reset();
            while (stream.incrementToken()) {
                String token = stream.getAttribute(CharTermAttribute.class).toString();
                if (!map.containsKey(token)) {
                    map.put(token, 0);
                }
                map.put(token, map.get(token) + 1);
            }

            List<Entry<String, Integer>> entries = new ArrayList<>(map.entrySet());
            Collections.sort(entries, (Entry<String, Integer> o1, Entry<String, Integer> o2) -> -1 * (o1.getValue()).compareTo(o2.getValue()));
            try (FileWriter writer = new FileWriter(output)) {
                for (Entry<String, Integer> entry : entries) {
                    writer.write(entry.getKey() + "," + entry.getValue() + "\n");
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }

    }
}
